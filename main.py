#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 27 08:38:43 2019

@author: carlelgcrona
"""

import os
import logging
import json
import sys
import datetime

from src import utils
from src import fetcher
from src import parser
from src import html_gen
from src import index

# Get the current datetime
time_now = str(datetime.datetime.now())

# Create logger
logger = utils.create_logger()
# Logger init
logger = logging.getLogger(__name__)
# Start the logging
logger.info('Started! | {}'.format(time_now))

#############################
### USER DEFINED VARIABLE ###
#############################

# Mode the program should operate in
mode = "interactive"


overwrite = False

# Book scrape
folder_name = "interactive" # Matte 5

# Student scrape
book = "ma5000"
classid = "55298" 
coursetype = "u"
student_id = "728140"


# Interactive scrape
assignment_id = None
assignment_id = "210696" # KNAS

selected_assignment_id = 50000
how_many_loops = 10


#############################
#                           #
#############################
#%%
# Init
base_path = utils.init_structure(mode, folder_name, student_id) # Inits the 
logging.info("Base Path: {}".format(base_path))
logging.info("Program Mode: {}".format(mode))
index_file_path = "{}/index.html".format(base_path)
auth = utils.load_auth() # Load the auth file


# Book mode
if mode == "book_scrape":

    # Generates an index
    assignments = index.generate_book_index(folder_name)

    #Loop over all assignments
    for k,x in enumerate(assignments):

        logging.info("Assignement: {}/{}".format(k+1, len(assignments)))
        auth = fetcher.scrape_assignment(assignments[k], base_path, 
                                index_file_path, auth)

# Interactive mode
elif mode == "interactive":

    for assignment_id in range(selected_assignment_id, selected_assignment_id + how_many_loops):
        
        while assignment_id == None:

            logging.debug("Assignement_id == None --> Asking for assignment_id")
            assignment_id = input("Input a valid Assignement_id: ")

        auth = fetcher.scrape_assignment(assignment_id, base_path, 
                                    index_file_path, auth, overwrite)

    # Generate index
    index.generate_index_2(base_path)

# Student scrape
elif mode == "student_scrape":

    # Get all assignments for the student
    assignments = fetcher.get_assingments_for_student(book, classid, coursetype, student_id)

    #Loop over all assignments
    for k,x in enumerate(assignments):

        logging.info("Assignement: {}/{}".format(k+1, len(assignments)))
        auth = fetcher.scrape_assignment(assignments[k], base_path, 
                                index_file_path, auth, overwrite)

    # Generate index
    index.generate_index(base_path)

# Debug
elif mode == "debug_mode":

    html_data = fetcher.debug_load(assignment_id, base_path, auth, overwrite)
    auth = fetcher.scrape_assignment(assignment_id, base_path, 
                                index_file_path, auth, overwrite)

    # Generate index
    index.generate_index(base_path)


time_end = str(datetime.datetime.now())
logging.info("Finished! | {}".format(time_end))
