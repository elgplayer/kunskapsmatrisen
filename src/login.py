# -*- coding: utf-8 -*-

import os
import json
import logging
import re
import datetime

import requests
from bs4 import BeautifulSoup

# Logger init
logger = logging.getLogger(__name__)


def get_token(auth):

    logging.info("Fetching new token...")

    # Gets the csrfmatch from the HTML
    url = "https://www.kunskapsmatrisen.se/logga-in.php"
    
    headers = {
        'dnt': "1",
        'upgrade-insecure-requests': "1",
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
        'sec-fetch-user': "?1",
        'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        'cache-control': "no-cache"
        }
    
    s = requests.Session()
    r = s.get(url, headers=headers)
    
    data = r.text
    data_soup = BeautifulSoup(data, 'html.parser')
    
    # Extract the csrfmatch
    csrfmatch = data_soup.find("input", {"id": "csrfmatch"})['value'] 
    
    # Usese the csrfmatch + credentials to login to get nice cookies
    url = "https://www.kunskapsmatrisen.se/login.php"
    payload = "csrfmatch={}&logintype=2&optionsRadios1=2&optionsRadios2=2&optionsRadios3=2&pincode={}&useremail={}".format(csrfmatch, auth['password'], auth['username'])
    
    headers = {
        'Origin': "https://www.kunskapsmatrisen.se",
        'Upgrade-Insecure-Requests': "1",
        'DNT': "1",
        'Content-Type': "application/x-www-form-urlencoded",
        'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
        'Sec-Fetch-User': "?1",
        'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        'cache-control': "no-cache",
        }
    
    # Post the data 
    s.post(url, data=payload, headers=headers)
    
    # Uses the obtained cookies to request https://www.kunskapsmatrisen.se/elev/. By using beutifulsoup, we can to extract the "token" string
    url = "https://www.kunskapsmatrisen.se/elev/"
    
    headers = {
        'origin': "https://www.kunskapsmatrisen.se",
        'upgrade-insecure-requests': "1",
        'dnt': "1",
        'content-type': "application/x-www-form-urlencoded",
        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
        'sec-fetch-user': "?1",
        'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        'cache-control': "no-cache"
        }
    
    # Get the website and parse it as a soup
    response = s.get(url, headers=headers)
    response_text = response.text
    response_soup = BeautifulSoup(response_text, 'html.parser')
    
    # Find all script tags
    script_tags  = response_soup.find_all("script")
    
    # Selects the second script tag
    data_tag = script_tags[1].string
    
    # Checking that we got a response that we wanted
    if data_tag == None:
        
        logging.critical("INVALID_AUTHENTICATION")
        raise "INVALID_AUTHENTICATION"
    
    # Search in the second script tag after "var token = " with RegEx
    token = re.search(r"var token = (.*?);", data_tag).group(1)
    
    # Remove ' from the token
    token = token.replace("'", "")
    
    # Add bearer infront of the token
    logging.info("Token fetched!") 
    token = "Bearer {}".format(token)

    
    return token