# -*- coding: utf-8 -*-

import os
import json
import pickle
import logging
from datetime import date

import requests

from src import utils
from src import html_gen

# Logger init
logger = logging.getLogger(__name__)


def get_book(book_name, overwrite=False):
    '''
    Gets all assignments in a book
    
    Attributes:

        * book_name (str): Unqie book_name (e.g: ma5_10_ma5000)
        * overwrite (Boolean): Should the book be overwritten?
        
    return: Response object
    '''

    file_path = "output/{}/responses/{}.pkl".format(book_name, book_name)
    
    if (utils.check_exists(file_path) == 1 and overwrite == False):
        
        response = pickle.load(open(file_path, 'rb'))
        
        return response

    url = "https://www.kunskapsmatrisen.se/js/books/{}.js".format(book_name)
    
    headers = {
        'cache-control': "no-cache",
        }
    
    response = requests.request("GET", url, headers=headers)
    logging.info("Downloading book: {}".format(book_name))
    
    # Pickle the response
    pickle.dump(response, open(file_path, 'wb'))


    return response


def organize_hier(book_json):
    '''
    Organize all assignments according to their hierarchy 

    Attributes:

        * book_json (json): The book as JSON

    returns: Assingment list (dict)
    '''

    assignment_list = {}
    
    # Get the main hierarchy
    for k,x in enumerate(book_json['momtable']):
        
        main_hir = book_json['momtable'][k]['vcc']
        assignment_list[main_hir] = {}
    
    # Get the second ordet of the hierarchy
    for k,x in enumerate(book_json['momtable']):
        
        # Create grade info string
        grade_abv = book_json['momtable'][k]
        grade_str = "E: {} | C: {} | A: {}".format(grade_abv['e'], grade_abv['c'], grade_abv['a'])
        
        # Add the shiet to the main hier
        main_hir = book_json['momtable'][k]['vcc']
        second_hir = book_json['momtable'][k]['vmoment']
        assignment_list[main_hir][second_hir] = {'info': grade_str, 'list': []}
        
        # Add the all the assignments to a list grouped by hierarchy
        for kx, ky in enumerate(book_json['momtable'][k]['u']):
            
            assignment_list[main_hir][second_hir]['list'].append(book_json['momtable'][k]['u'][kx][0])
    

    return assignment_list
    

def generate_book_index(book_name):
    '''
    Generates the actual index for a given book

    Attributes:

        * book_name (str): The books name

    return: HTML 
    '''
    
    index_file_path = "output/{}/index.html".format(book_name)
    assignment_path = os.getcwd() + "/output/{}/html".format(book_name)

    # Get the book and parse it as JSON
    book = get_book(book_name)
    book_json = book.json()
    
    # Make a clear hierarchy for the book
    assignment_list = organize_hier(book_json)
    assignments = []

    # Init the HTML with an boilerplate
    html = html_gen.index_boilerplate()

    # Go through the assignment_list
    for k,x in enumerate(assignment_list):

        # Create main header
        html += html_gen.create_header(1, x)

        # Create secondary header
        for kx, ky in enumerate(assignment_list[x]):

            try:

                # Create secondary header
                html += html_gen.create_header(2, ky)
                html += html_gen.create_header(3, assignment_list[x][ky]['info'])
                
                for kx2, ky2 in enumerate(assignment_list[x][ky]['list']):

                    file_name = assignment_list[x][ky]['list'][kx2]
                    assignment_file_path = "{}/{}.html".format(assignment_path, ky2)
                   
                    
                    html += html_gen.create_link(assignment_file_path, file_name)

                    # Add all assignments to a list
                    assignments.append(str(assignment_list[x][ky]['list'][kx2]))

            except:

                pass         

    # Write the HTML to a file
    html_gen.write_to_file(index_file_path, html, "INDEX WRITTEN")
    

    return assignments


def generate_index(base_path):
    '''
    Generates the actual index for a given book

    Attributes:

        * base_path (str): Base of the path

    '''

    index_file_path = "{}/index.html".format(base_path)
    html_folder = '{}/html'.format(base_path)
    absolute_path = os.path.abspath(html_folder)

    # Get all files in the directory
    files = os.listdir(html_folder)

    # Goes through the list to strip the .html file to sort and convert to int
    for k,x in enumerate(files):
        files[k] = int(files[k][:-5])
    
    # Sort the list
    files.sort()

    # Convert the assingment_id to str
    for k,x in enumerate(files):
        files[k] = str(files[k])

    # Init the HTML with an boilerplate
    html = html_gen.index_boilerplate()

    # Create main header
    html += html_gen.create_header(1, "Interactive kunskapsmatrisen index")

    # Go through the assignment_list
    for k,x in enumerate(files):

        file_name = files[k]
        
        assignment_file_path = "{}/html/{}.html".format(base_path, file_name)

        html += html_gen.create_link(assignment_file_path, file_name)

    # Write the HTML to a file
    html_gen.write_to_file(index_file_path, html, "Index written to file: {}".format(index_file_path))


def generate_index_2(base_path, index_limit=1000):
    '''
    Generates an index with indexes too avoid lag because of too many links

    Attributes:

        * base_path (str): Base of the path
        * index_limit (int): How many assignments should be in each "chunk"
    '''

    main_index_path = "{}/index.html".format(base_path)
    html_folder = '{}/html'.format(base_path)
    absolute_path = os.path.abspath(html_folder)
    index_path = "{}/index".format(base_path)
    
    # Get all files in the directory
    files = os.listdir(html_folder)
    
    # Goes through the list to strip the .html file to sort and convert to int
    for k,x in enumerate(files):
        files[k] = int(files[k][:-5])
    
    # Sort the list
    files.sort()
    
    # Convert the assingment_id to str
    for k,x in enumerate(files):
        files[k] = str(files[k])

    # Split the files list into equally spaced chunks
    chunked_files = [files[i:i + index_limit] for i in range(0, len(files), index_limit)]

    # Init the HTML with an boilerplate
    html = html_gen.index_boilerplate()
    # Back to main index
    html += html_gen.create_link(main_index_path, "Back to index", 1)
    # Create main header
    html += html_gen.create_header(1, "Interactive kunskapsmatrisen index")

    # Go through the chunked files list
    for k,x in enumerate(chunked_files):

        sel_list = chunked_files[k]

        first_assignment = sel_list[0]
        last_assignment = sel_list[-1]
        number_of_files = len(sel_list)

        main_index_file_str = "{} - {}   ({})".format(first_assignment, last_assignment, number_of_files)
        index_file_path = "{}/{}.html".format(index_path, k)

        # Create the index
        create_index(base_path, sel_list, main_index_path, index_file_path)

        # Creat link to the index
        html += html_gen.create_link(index_file_path, main_index_file_str)

    # Write the HTML to a file
    html_gen.write_to_file(main_index_path, html, "Index written to file: {}".format(main_index_path))



def create_index(base_path, tmp_list, main_index_path, index_file_path):
    
    
    # Init the HTML with an boilerplate
    html = html_gen.index_boilerplate()

    # Back to main index
    html += html_gen.create_link(main_index_path, "Back to index", 1)

    # Create main header
    html += html_gen.create_header(1, "Interactive kunskapsmatrisen index")

    # Go through the assignment_list
    for k,x in enumerate(tmp_list):

        file_name = tmp_list[k]

        assignment_file_path = "{}/html/{}.html".format(base_path, file_name)
        #link_text = "{} - {}".format(file_name_display, date_today)

        html += html_gen.create_link(assignment_file_path, file_name)

    # Write the HTML to a file
    html_gen.write_to_file(index_file_path, html, "Index written to file: {}".format(index_file_path))
