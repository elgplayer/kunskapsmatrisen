# -*- coding: utf-8 -*-
from string import ascii_lowercase

from src import fetcher
from src import utils


def get_picture_name(text):
    '''
    Gets the name of a picture by parsing the HTML

    Attributes:
        * text (str): HTML string that contains the picture name

    returns: The picture name
    '''   
    

    question_str_split = text.split('"')
    file_names = []
    
    for k,x in enumerate(question_str_split):
        
        if ("/uppgiftsbilder/htmlimg/" in question_str_split[k]):
            picture_str = question_str_split[k].split("/")
            file_name = picture_str[-1]
            
            file_names.append(file_name)


    return file_names


def get_src_url(text):
    '''
    Gets the URL link to the image that is recivied from kunskapsmatrisen.

    Attributes:

        * text (str): HTML string that contains the picture link

    returns: List with all src URLs of solutions
    '''  
    src_split = text.split("src")
    url_list = []
    
    for k,x in enumerate(src_split):
        
        if "/ckeditor/" in src_split[k]:
            
            ckeditor = src_split[k]
            
            tmp = ckeditor.split('="')
            
            for kx, ky in enumerate(tmp):
                
                if "/ckeditor/plugins/ckeditor_wiris/integration" in tmp[kx]:
                    
                    tmp_2 = tmp[kx]
                    tmp_2 = tmp_2.replace('" width', '')
                    
                    url_list.append(tmp_2)
                    

    return url_list


def get_url_picture(text):
    '''
    Adds kunskapsmatrisen.se to each link in the list so they can be downloaded

    Attributes:

        * text (str): HTML string that contains the picture link

    returns: List with all src URLs of solutions with kunkspasmatrisen appended to it
    '''  

    url_list = get_src_url(text)    
    
    for k,x in enumerate(url_list):
        
        # Add kunskapsmatrisen.se
        url_list[k] = "https://kunskapsmatrisen.se{}".format(url_list[k])
    
    
    return url_list


def get_hash_name(url):
    '''
    Takes an url and gets the hash of the the link

    Attributes:

        * url (str): HTML string that contains the picture link

    returns: Hash of the formula (str) e.g.: 4f45a9c171ef673d0ee086c36c8eb8c6
    '''
    hash_name = ""
    tmp = url
    
    tmp_2 = tmp.split('?formula=')
    tmp_3 = tmp_2[1].split('&')
    hash_name = tmp_3[0]
    
    
    return hash_name


def replace_answer_picture(base_path, text):
    '''
    Replaces the default picture URL to an link to the local hard drive with the hash as the file name

    Attributes:

        * base_path (str): Base of the path
        * url (str): HTML string that contains the picture link

    returns: Text with all paths replaced
    '''

    fetcher.get_answer_picture(base_path, text)
    pictures = get_src_url(text)


    for k,x in enumerate(pictures):
        
        url = pictures[k]
        hash_name = get_hash_name(url)

        path = "{}attachments/{}.png".format(base_path, hash_name)
        text = text.replace(url, path)
        
    return text


def num_to_letter(number):
    '''
    Gets the letter that corresponds to its index in the alphabet from 1

    Attributes:

        * number (int): Number index to convert to a character. e.g: 1 --> A, 2 --> B

    return: Converted number
    '''
    
    return chr(ord('@')+number)


def replace_input(text, answer):
    '''
    Replaces the fill in gap input text box with the answer

    Attributes:

        * text (str): Text where the input box should be removed from
        * answer (str): Answer that will be inserted before the "enhet". E.g "4" m

    return: Parsed text
    '''
    
    return answer + "" + text.split(">")[1]


























