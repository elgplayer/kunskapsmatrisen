# -*- coding: utf-8 -*-

import webbrowser
import os
import logging
import platform

from src import utils

# Get the loggers name
logger = logging.getLogger(__name__)



def parse_math_tag(str_input):
    '''
    Replaces invalid MathML scheme tags with a just a math tag

    Attributes:

        * str_input (str): Text to convert

    return: Parsed string
    '''     
    
    str_input = str_input.replace(
            '<math xmlns="http://www.w3.org/1998/Math/MathML">', '<math>')
    str_input = str_input.replace('<section>', '')
    str_input = str_input.replace('</section>', '')
    
    return str_input


def parse_linebreak(str_input):
    '''
    Replaces invalid newspaces tags with valid ones

    Attributes:

        * str_input (str): Text to convert

    return: Parsed string
    '''        
    str_input = str_input.replace('<mspace linebreak="newline">', 
                                  "<mspace linebreak='newline' />")
    
    return str_input
    

def remove_div(str_input):
    '''
    Removes div tags from math XML

    Attributes:

        * str_input (str): Text to convert

    return: Parsed string
    '''    

    str_input = str_input.replace('<div>', '')
    str_input = str_input.replace('</div>', '')
    
    return str_input


def mathml_converter(selected_text):
    '''
    Creates a HTML header given text and size

    Attributes:
        * selected_text (str): Text to convert

    return: Parsed and cleaned up XML
    '''
    
    new_text = selected_text
    new_text = parse_math_tag(new_text)
    new_text = parse_linebreak(new_text)
    new_text += "\n"
    

    return new_text


def html_boilerplate():
    '''
    Returns the base of the HTML boilerplate

    return: HTML boilerplate (str)
    '''

    base_html = '''\
<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript"
            src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
        </script>

    <script>
        function hide_and_show(element) {
          var x = document.getElementById(element);
          if (x.style.display === "none") {
            x.style.display = "block";
          } else {
            x.style.display = "none";
          }
        }
    </script>
    </head>
    <body>
    '''
    
    return base_html


def index_boilerplate():
    '''
    Returns the base of the index boilerplate

    return: HTML boilerplate (str)
    '''

    base_html = '''\
<!doctype html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/main.css">
</head>
<body>
'''
    return base_html 


def add_text(text=""):
    '''
    Adds a linebreak to a text

    Attributes:

        * text (str): What to add a break too

    returns: HTML with the div (str)
    '''
    return "{}<br>".format(text)


def add_div_hide(text="", div_index=0, button_text="Show answer"):
    '''
    Adds a div tag with a javascript function and button that hides the element
    By default, the object is hidden

    Attributes:

        * text (str): What to hide
        * div_index (int): Unique ID for each div tag
        * button_text (str): What the text button should say

    returns: HTML with the div (str)
    '''
    
    return '<button onclick="hide_and_show({})">{}</button><div style="display: none;" id="{}">{}'.format(div_index, button_text, div_index, text)


def add_div_close():
    '''
    Adds a closing div tag

    returns: HTML closing div tag (str)
    '''

    return "</div>"


def create_header(header_size, header_text):
    '''
    Creates a HTML header given text and size

    Attributes:
        * header_text (str or num): Text to set as header
        * header_size (int): How large the text should be 
                                [1 largest --> 5 smallest]    
    '''
    
    header_html = '<h{}> {} </h{}>'.format(header_size, header_text, header_size)
    
    return header_html


def create_link(link_path, link_name, file=1):
    '''
     Creates HTML links

    Attributes:

        * link_path (str): Where the file is located
        * link_name (str): What the link should display
        * file (str): Boolean value if the link points to a file or not

    return: Link (str)   
    '''

    if file == 1:

        link_path = 'file://' + link_path

    return'<a href="{}">{}</a><br>'.format(link_path, link_name)


def creat_image(file_path):
    '''
    Inserts image HTML

    Attributes:

        * file_path (str): Where the file is located'

    returns: HTML with the image embedded (str)
    '''

    return '''<img src="{}">'''.format(file_path)
    

def write_to_file(file_path, html, 
                  logger_msg="HTML written to file"):
    '''
    Writes the HTML to an file

    Attributes:

        * file_path (str): Where the file is located
        * html (str): HTML string that will be written to the file
        * logger_msg (str): What should be written to the log

    '''


    # Writes to file
    with open(file_path, 'w', encoding='utf8') as f:

        f.write(html)
        logger.debug(logger_msg)


def open_webbroswer(url):
    '''
    Opens a chrome tab for a given URL

    Attributes:

        * url (str): Web address to open
        
    '''
    
    # Open the webbrowser
    logging.info("Opening webbrowser to URL: {}".format(url))

    # Mac OS
    if platform.system() == "Darwin":
    	chrome_path = 'open -a /Applications/Google\ Chrome.app %s'
    	webbrowser.get(chrome_path).open(url)
    else:
    	webbrowser.get().open(url)