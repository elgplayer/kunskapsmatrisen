#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pickle
import logging

import requests

from src import utils
from src import parser
from src import html_gen
from src import string_parser

logger = logging.getLogger(__name__)


def get_solution(assignment_id, base_path, auth, overwrite=False):
    '''
    Gets the solution for a given assignment
    Pickles the response if the file does not exists
    
    Attributes:

        * assignment_id (str): Unquie assignment identifier. eg: "226260"
        * base_path (str): Base of the path
        * auth (str): Cookie authoritazation string
        * overwrite (Boolean): Should the assignment be overwritten? 
    
    return: Response object (Assignment solution)
    '''
    token = auth['token']
    response_name = "{}/responses/{}.pkl".format(base_path, assignment_id)

    # Check if the file exists or if it should by overwritten
    if ((os.path.exists(response_name)) and (overwrite == False)):

        response = pickle.load(open(response_name, 'rb'))
        logging.info("Response: {} loaded from file!".format(assignment_id))
        return_dict = {'response': response, 'loaded_from_file': 1}

        return return_dict
    
    logging.debug("Downloading solution: {}".format(assignment_id))

    url = "https://apikunskapsmatrisen1.azurewebsites.net//uppgifter/{}".format(assignment_id)
    
    querystring = {"fields":"uppgiftid,digitalaverktyg,epoints,cpoints,apoints,pointcomposition,htmlarr,solutionsource,litids","limit":"2"}
    
    headers = {
        'accept': "*/*",
        'origin': "https://www.kunskapsmatrisen.se",
        'authorization': token,
        'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
        'dnt': "1",
        'cache-control': "no-cache",
        }
    
    response = requests.request("GET", url, headers=headers, params=querystring)
    logging.debug("RESPONSE: {}".format(response))
    
    # Pickle the response
    pickle.dump(response, open(response_name, 'wb'))
    return_dict = {'response': response, 'loaded_from_file': 0}

    return return_dict


def download_picture_assignment(base_path, picture_name):
    '''
    Gets a picture that is related to the assignment
    
    Attributes:

        * base_path (str): Base of the path
        * picture_name (str): Unquie picture identifier. eg: "1_1436509668.png"  
        
    return: Response object
    '''
    
    url = "https://www.kunskapsmatrisen.se/uppgiftsbilder/htmlimg/{}".format(picture_name)
    path = "{}/attachments/{}".format(base_path, picture_name)

    if (utils.check_exists(path) == 0):

        logging.info("Downloading picture_assignment for assignment")
        response = requests.get(url)

        if response.status_code == 200:
            with open(path, 'wb') as f:
                for chunk in response:
                    f.write(chunk)


def download_picture_solution(base_path, picture_name):
    '''
    Downloads a picture that is related to the assignment
    
    Attributes:

        * base_path (str): Base of the path 
        * picture_name (str): Unquie picture name. eg: "1_1466156229.png"  
        
    '''    
    url = "https://www.kunskapsmatrisen.se/solutions/{}".format(picture_name)
    path = "{}/attachments/{}".format(base_path, picture_name)
    
    if (utils.check_exists(path) == 0):

        logging.info("Downloading download_picture_solution for assignment")
        response = requests.get(url)

        if response.status_code == 200:
            with open(path, 'wb') as f:
                for chunk in response:
                    f.write(chunk)


def download_firebase_picture(base_path, url, file_name):
    '''
    Downloads the pictures from google firebase and writes the pictures to disk

    Attributes:

        * base_path (str): Base of the path
        * url (str): The URL to scrape, e.g: https://firebasestorage.googleapis.com/v0/b/question-images.appspot.com/o/1568976391837sSaJ0Vm?alt=media&token=7f2565fb-26fa-4bfc-b974-956932624637
        * file_name (str): What should the file be saved as? e.g: 1568976391837sSaJ0Vm.png
    '''
    
    path = "{}/attachments/{}".format(base_path, file_name)
    
    if (utils.check_exists(path) == 0):

        logging.info("Downloading download_firebase_picture for assignment")
        response = requests.get(url)

        # Downloads the pictures
        if response.status_code == 200:
            with open(path, 'wb') as f:
                for chunk in response:
                    f.write(chunk)        


def get_firebase_picture_name(base_path, text):
    '''
    Gets the pictures from google firebase and replaces the links to a local link

        * base_path (str): Base of the path'
        * text (str): Text to parse

    return: HTML text with the links replaceed-
    '''
    
    src_split = text.split("src")

    # Go through the list
    for k,x in enumerate(src_split):

        # Skip the first item
        if k != 0:
            
            url = src_split[k].split('" style="')[0][2:] # Gets the URL
            new_url = url.replace("amp;", "") # Removes amp so the URL works

            try:
                # Gets the filename of the picture
                file_name = url.split("/o/")[1].split("?")[0] + ".png" 

                # Create the local file URL
                local_file_url = "{}/attachments/{}".format(base_path, file_name) 
                #print(local_file_url)

                # Replaces the link to GOOGLE to the local link
                text = text.replace(url, local_file_url)
                
                # Download the pictures 
                download_firebase_picture(base_path, new_url, file_name)
                
            except:

                pass
            
    return text
            

def get_picture_assignment(base_path, text):
    '''
    Gets all pictures for the solution
    
    Attributes:

        * base_path (str): Base of the path 
        * text (str): Answer string that will be used to get all the pictures URL

    return: Text with the image link pointing to a downloaded version of the picture (str)

    '''    

    # Downloads pictures
    if ("/uppgiftsbilder/htmlimg" in text):

        logging.debug("Picture in answer")

        # Parse the picture name
        picture_names = string_parser.get_picture_name(text)

        for picture_name in picture_names:
            
            # Download the picture
            download_picture_assignment(base_path, picture_name)

            # Replace the orignal path with the new path
            new_path = "{}/attachments/".format(base_path)

            text = text.replace("/uppgiftsbilder/htmlimg/", new_path)


    return text    
        

def get_answer_picture(base_path, text):
    '''
    Downloads pictures that is related to the assignment
    
    Attributes:

        * picture_name (str): Unquie picture identifier. eg: "sol-db-226260-22-46.png"
        * text (str): Answer string that will be used to get all url's

    '''    

    url_list = string_parser.get_url_picture(text)

    # Goes through the list
    for k,x in enumerate(url_list):

        # Set the URL
        url = url_list[k]

        # Get the hash name
        hash_name = string_parser.get_hash_name(url)

        # Set the path
        path = "{}/attachments/{}.png".format(base_path, hash_name)

        #logging.info(url)

        if (utils.check_exists(path) == 0):

            # Download the picture
            response = requests.get(url, verify=False)

            logging.info("Downloading answer_picture for assignment")

            if response.status_code == 200:
                with open(path, 'wb') as f:
                    for chunk in response:
                        f.write(chunk)    
        

def get_book(book_name):
    '''
    Gets all assignments in a book
    
    Attributes:

        * book_name (str): Unqie book_name (e.g: ma5_10_ma5000)
        
    return: Response object
    '''

    url = "https://www.kunskapsmatrisen.se/js/books/{}.js".format(book_name)
    
    headers = {
        'cache-control': "no-cache",
        }
    
    response = requests.request("GET", url, headers=headers)
    
    
    return response
        

def get_assingments_for_student(book="ma5000", classid="55298", 
                                coursetype="u", student_id="728140"):
    '''
    Gets all assignment for a particular student and returns a list with the assignments
    
    Attributes:
        
        * book (str) [Default=ma5000]: What book to the student have
        * classid (str) [Default=55298]: What class the student belongs to
        * coursetype (str) [Default=u]: Coursetype
        * student_id (str) [Default=728140]: The student unique id
        
        
    return: List with all assignment_id's (list)
    '''
    
    url = "https://www.kunskapsmatrisen.se/service/getuppgift.php"
    

    payload = "book={}&classid={}&coursetype={}&getdonesforstudentid={}".format(book, classid, 
                                                                                coursetype, student_id)
    
    headers = {
        'Accept': "*/*",
        'Origin': "https://www.kunskapsmatrisen.se",
        'X-Requested-With': "XMLHttpRequest",
        'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
        'DNT': "1",
        'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8",
        'Cache-Control': "no-cache",
        'Postman-Token': "857d63ce-f1c5-46b6-8dfd-3ec81046e3e3,4e2a1cdb-6e5d-4c3a-af57-7569e10f16a9",
        'Host': "www.kunskapsmatrisen.se",
        'Accept-Encoding': "gzip, deflate",
        'Content-Length': "66",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
        }
    
    response = requests.request("POST", url, data=payload, headers=headers)
    
    json_data = response.json()
    assingments = []
    
    for k,x in enumerate(json_data):
        
        if len(json_data[k]) > 0:
            
        
            assingments.append(json_data[k][1])

    logging.info("{} Assignments for studnet: {}".format(len(assingments), student_id))
    
    
    return assingments


def scrape_assignment(assignment_id, base_path, 
                      index_file_path, auth, overwrite=False):
    '''
    A function that calls other functions witch collecvlty fetches 
    and parses all data to generate a nice HTML file

    Attributes:

        * assignment_id (str): What assignment was scraped? e.g: 258139
        * base_path (str): Base of the path
        * index_file_path (str): Where should the index file path be?
        * auth (str): String with a cookie to authenticate some requests at kunskapsmatrisen
        * overwrite (Boolean) [default=False]: Should a potentially exisiting response be overwritten?
        
    ''' 
    
    logging.info("Beginning assignment: {}".format(assignment_id))

    # Init the path
    assignment_path = "{}/html/{}.html".format(base_path, assignment_id)

    # Downloads the assignment
    assignment_response = get_solution(assignment_id, base_path, auth, overwrite)
    
    # Parse the assignemt
    parsed_html = parser.parse_assignment(base_path, assignment_response['response'], assignment_id, assignment_response['loaded_from_file'])

    # Try again if the token is invalid
    if parsed_html == "FETCHING_TOKEN":
        
        # Gets a new token
        auth = utils.fetch_new_token("EXPIRED")    

        # Downloads and parses the assignment again
        assignment_response = get_solution(assignment_id, base_path, auth, True)
        parsed_html = parser.parse_assignment(base_path, assignment_response, assignment_id)
    
    # Check again, if still empty --> Skip
    if parsed_html == "EMPTY_RESPONSE":
        
        return auth

    # Init the HTML
    html = html_gen.html_boilerplate()

    # Add the Link back to Index and Task ID header
    html += html_gen.create_link(index_file_path, "Back to index", 1)
    html += html_gen.create_header("1", "Uppgift {}".format(assignment_id))
    html += parsed_html

    html_gen.write_to_file(assignment_path, html, "Solution HTML generated")
    logging.info("Assignment {} finished!".format(assignment_id))


    return auth



def debug_load(assignment_id, base_path, auth, overwrite):
    
    assignment_response = get_solution(assignment_id, base_path, auth, overwrite)
    #print(assignment_response)
    
    html_data = parser.html_data_load(assignment_response['response'])
    
    return html_data
    

    
    
    