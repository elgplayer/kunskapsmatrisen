#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 27 09:20:27 2019

@author: carlelgcrona
"""
import json
import os
import logging
import sys

from src import utils
from src import fetcher
from src import html_gen
from src import string_parser

# Logger init
logger = logging.getLogger(__name__)


def parse_assignment(base_path, assignment_response, 
                     assignment_id, loaded_from_file=0):
    '''
    Parses the responses as JSON and stiches the responses togheter to an HTML file to be viewed later

    Attributes:

        * base_path (str): Base of the path
        * assignment_response (Response object): Response of a request to get an assignment
        * assignment_id (str): What assignment was scraped? e.g: 258139
        * loaded_from_file (Boolean) [default=0]: Is the assignment loaded from file or not?

    return: HTML text
    '''

    div_index = 0

    if "loaded_from_file" in assignment_response:

        data_json = assignment_response['response'].json()

    else:

        try:

            data_json = assignment_response.json()
            
        except Exception as e:

            logging.warning("JSON parsing error: {}".format(e))

            return "EMPTY_RESPONSE"

    # Check for error
    if ("status" in data_json):

        if (data_json['status'] == "error"):

            logging.warning("ERROR: {}\n".format(data_json['message']))
            
            if data_json['message'] == "Token not found" or data_json['message'] == "Expired token":
                
                utils.fetch_new_token("EXPIRED")

                return "FETCHING_TOKEN"

            if data_json['message'] == "Unexpected control character found" or "Signature verification failed":

                return "FETCHING_TOKEN"    
            
            utils.write_error(base_path, data_json, assignment_id, "json")
            logging.info("Skipping...")
            
            return "ERROR BIG TIME"

    #if loaded_from_file == 1

    if data_json['sqldata'] == []:
    
        logging.info("The response does not contain any data")

        return "EMPTY_RESPONSE"


    # Assignment id
    assignment_id = data_json["uppgiftid"]
    
    # Data abbrivation
    assignment_data = data_json["sqldata"][0]
    
    # HTML data
    html_data = json.loads(assignment_data["htmlarr"])
    #utils.write_error(base_path, html_data, assignment_id, "pkl")
    
    ## POINT INFO ##§
    point_str = "Poäng: "
    
    # A Points
    if assignment_data["apoints"] != 0:
        point_str += "A: {} | ".format(assignment_data["apoints"])
    
    # C Points
    if assignment_data["cpoints"] != 0:
        point_str += "C: {} | ".format(assignment_data["cpoints"])
        
    # E Points
    if assignment_data["epoints"] != 0:
        point_str += "E: {}".format(assignment_data["epoints"])
    
    # Removs traling "|"
    if ("| " == point_str[-2:]):
        point_str = point_str[:-3]        
        
        
    ## POINT COMPESITION ##
    point_composition = "Kriterier: " + assignment_data["pointcomposition"]
    
    ## HTML DATA ##  
    html_text = ""
    
    html_text += html_gen.create_header("2", "Misc")
    html_text += html_gen.add_text(point_str)
    html_text += html_gen.add_text(point_composition)
    
    # Goes through the data
    for k,x in enumerate(html_data):
        
        text_info = ""
        question = ""
        answer = ""
        
        # Abbrevation
        sel_data = html_data[k]
        question_type = sel_data['type']

        # Description of Assignment
        if (question_type == "informationBlock"):
            
            #question = fetcher.get_picture_assignment(base_path, question)
            if 'question' in sel_data['data']:
                    
                question += sel_data['data']['question']
                
            html_text += html_gen.create_header("2", "Problem")
            html_text += html_gen.add_text(question)
            
        # If it is a question
        if (sel_data["isQuestion"] == True):
            
            # Abbrivation
            question_data = sel_data['data']
            question = question_data['question']
            
            text_info += "{} - {}".format(sel_data["number"], sel_data["point"])
            html_text += html_gen.create_header("3", text_info)

            if "multipleChoice" in question_type:
                
                question += html_gen.add_text("")
                
                for k,x in enumerate(question_data['multipleChoiceAnswer']):
                    
                    selected_multi_quest = question_data['multipleChoiceAnswer'][k]
                    
                    multi_question_id = string_parser.num_to_letter(selected_multi_quest['id'])
                    multi_question = selected_multi_quest['label']
                    is_correct = selected_multi_quest['isCorrect']
                    multi_question_str = "{} - {}".format(multi_question_id, multi_question)
                    
                    question += html_gen.add_text(multi_question_str)
                    
                    if is_correct == True:
                                                
                        answer += html_gen.add_text(multi_question_str)
                        
                        if "multipleChoice" in question_type:
                
                            question += html_gen.add_text("")
                
            if "flexInput" in question_type:

                for k,x in enumerate(question_data['flexInputAnswer']):

                    selected_multi_quest = question_data['flexInputAnswer'][k][0]
                    multi_question_str = "{}".format(selected_multi_quest)
                    answer += html_gen.add_text(multi_question_str)
                        
            # Formatted input
            if "formatedInput" == question_type:

                answer_str = ""
                answer_data = question_data['formatedInputAnswer']
                answer_data_2 = ""
                
                answer_str = question_data['input']
                
                if type(answer_data) == list:

                    for k,x in enumerate(answer_data):

                        answer_data_2 += answer_data[k][0] + ", "
                    
                    answer_data_2 = answer_data_2[:-2]

                else:

                    answer_data_2 = answer_data     

                answer_str = string_parser.replace_input(answer_str, answer_data_2)
                answer += answer_str
                               
            if "simpleAnswer" == question_type:
                
                answer_str = ""
                answer_data = question_data['simpleAnswerAnswer'][0]

                if type(answer_data) == list:

                    for k,x in enumerate(answer_data):

                        answer_str += answer_data[k][0]
                else:

                    answer_str = answer_data

                answer += "Answer: {}".format(html_gen.add_text(answer_str))


            if "fillGaps" in question_type or "flexInputAnswer" in question_type:
                
                if 'formatedInputAnswer' in question_data: 
                    
                    answer_list = question_data['formatedInputAnswer']
                    answer_str = ""
                    
                    for kx2, ky2 in enumerate(answer_list):
                        
                        for i in answer_list[kx2]:
                            
                            answer_str += html_gen.add_text(i)
                        
                    answer += "Answer: <br>{}".format(html_gen.add_text(answer_str))
                    
                    
            if "freeText" == question_type:

                answer_str = ""

                if "freeTextAnswer" in question_data:

                    answer_list = question_data['freeTextAnswer']

                    if type(answer_list) == list:
                    
                        for kx2, ky2 in enumerate(answer_list):
                            
                            answer_str += html_gen.add_text(answer_list[kx2][0]) 

                    else:

                        answer_str = answer_list

                answer += answer_str
                
            # Problem
            html_text += html_gen.create_header("2", "Problem")
            html_text += question

            # Answer
            html_text += html_gen.create_header("3", "Answer")
            html_text += html_gen.add_div_hide("", div_index)
            div_index += 1
            html_text += answer
            
            # Criterias
            if "criterias" in question_data:

                html_text += html_gen.create_header("4", "Answer Criterias")
                criterias = ""
                
                # Goes through all criteras
                for kx,ky in enumerate(question_data["criterias"]):
                    
                    criterias += "Level: {} | ".format(question_data["criterias"][kx]['level'])
                    criterias += "Ability: {} | ".format(question_data["criterias"][kx]['ability'])
                    criterias += "Comment: {}".format(question_data["criterias"][kx]['comment'])
                    criterias += "<br>" # Nice linebreak
                
                # Appends all the criterias to the main HTML
                html_text += criterias

            # Downloads solution pictures
            if assignment_data["solutionsource"] != "":

                # Abbrivation
                solutionsource = assignment_data["solutionsource"]

                if solutionsource != None:

                    # Create header 
                    html_text += html_gen.create_header("4", "Answer solutions")

                    # Makes sure it is a list
                    if "|" not in solutionsource:

                        solutionsource = [solutionsource]

                    # Many solutionssoruce
                    if "|" in solutionsource:

                        solutionsource = solutionsource.split("|")

                    # Go through the list that prints out the solutions
                    for kx,ky in enumerate(solutionsource):

                        if "png" in solutionsource[kx] or "jpg" in solutionsource[kx]:

                                # File path
                                new_path = "{}/attachments/{}".format(base_path, solutionsource[kx])
                                
                                # Check if the solution is already downloaded
                                if (utils.check_exists(new_path) == 0):

                                    # Downloads the picture
                                    fetcher.download_picture_solution(base_path, solutionsource[kx])

                                html_text += html_gen.add_text("") # Adds a linebreak
                                html_text += html_gen.add_text("Solution: ") # Adds nice picture index
                                html_text += html_gen.add_div_hide(html_gen.creat_image(new_path), div_index, "View solution steps: {}".format(kx+1))
                                html_text += html_gen.add_div_close()
                                div_index += 1

        # Some simple HTML parsing
        html_text = html_gen.mathml_converter(html_text)
        html_text += html_gen.add_div_close()
        
    # Downloads firebase pictures
    if "firebasestorage" in html_text:
        
        html_text = fetcher.get_firebase_picture_name(base_path, html_text)

    if "/ckeditor/plugins/ckeditor_wiris/integration/showimage.php" in html_text:

        html_text = string_parser.replace_answer_picture(base_path, html_text)

    # Answers
    html_text = fetcher.get_picture_assignment(base_path, html_text)

    return html_text


def html_data_load(assignment_response):
    
    try:
        data_json = assignment_response.json()
    except Exception as e:
        print("ERROR: ", e)
        logging.warning("JSON parsing error...")

        return "EMPTY_RESPONSE"

    # Check for error
    if ("status" in data_json):

        if (data_json['status'] == "error"):

            logging.warning("ERROR: {}\n".format(data_json['message']))
            
            if data_json['message'] == "Token not found" or data_json['message'] == "Expired token":
                
                return "FETCHING_TOKEN"
            
            logging.info("Skipping...")
            
            return "ERROR BIG TIME"

    if data_json['sqldata'] == []:
    
        logging.info("The response does not contain any data")

        return "EMPTY_RESPONSE"

    # Data abbrivation
    assignment_data = data_json["sqldata"][0]
    
    # HTML data
    html_data = json.loads(assignment_data["htmlarr"])

    return html_data