# -*- coding: utf-8 -*-

import os
import sys
import logging
import datetime
import json
import base64
import re
import pickle

from src import login

# Logger init
logger = logging.getLogger(__name__)


def create_logger():
    '''
    Creates a logger to log the script
    
    return: Logger object
    '''
    
    #init_structure()
    root_logger = None
    
    
    date_today = datetime.datetime.today().strftime('%Y_%m_%d__%H_%M_%S')
    log_file_name = "log/{}.log".format(date_today)

    # Create folder if not exists
    if not os.path.exists("log/"):
        os.makedirs("log/", exist_ok=True)

    # Create logger
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    
    if root_logger.handlers:
        root_logger.handlers.pop()
        print("Removing old logger from memory...")
        
    # Create file handler and set level to debug
    fh = logging.FileHandler(log_file_name, mode = 'w')
    fh.setLevel(logging.DEBUG)
    
    # Create console handler with a higher log level
    ch = logging.StreamHandler(stream=sys.stdout)
    ch.setLevel(logging.INFO)
    
    # Create formatter and add it to the handlers
    formatter = logging.Formatter('[%(asctime)s] %(levelname)8s --- %(message)s ' +
                                  '(%(filename)s:%(lineno)s)', 
                                  datefmt='%Y-%m-%d %H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
        
    # Add the handlers to the logger
    root_logger.addHandler(fh)
    root_logger.addHandler(ch)
    

    return logger


def init_structure(mode, folder_name="", student_id=""):
    '''
    Inits the folder structure by creating all folders

    Attributes:

        * mode (str): What mode the program is in, e.g: interactive
        * folder_name (str) [default=""]: The name of the folder
        * student_id (str) [default=""]: Unqiue id for each student
    '''

    if mode == "interactive" or mode == "debug_mode":

        if folder_name == "":
            folder_name = "interactive"

    if mode == "student_scrape":

        folder_name = "student/{}".format(student_id)

    # Base path
    if folder_name != None or folder_name == "":
        base_path = os.getcwd() + "/output/{}".format(folder_name)
    else:
        base_path = os.getcwd() + "/output"

    # Base folders
    base_folder = ['log', 'src', 'output', 'deb']
    for folder in base_folder:
        
        # Check if the folder exists
        if not os.path.exists(folder):
            
            os.makedirs(folder, exist_ok=True)
            logging.info("Folder created: {}".format(folder))


    folder_struct = [
        'html', 'attachments', 'responses', 'error', 'index'
    ]


    for folder in folder_struct:

        if folder_name != "":

            folder = f"{base_path}/{folder}"     
        
        folder += "/"

        if not os.path.exists(folder):
            
            os.makedirs(folder, exist_ok=True)
            logging.info("Folder created: {}".format(folder))



    return base_path


def check_exists(file_path):
    '''
    Checks if a file exists

    Return: 0 if not exists, else 1
    ''' 
    if not os.path.exists(file_path):

        return 0

    return 1


def write_json(data, file_path, logger_msg="Data written to disk"):
    '''
    Writes JSON data to disk

    Attributes:
        * data (ANY): Data that will be written to file
        * file_path (str): Path where to store the data
        * logger_msg (str): Logger debug message
    '''

    # Open the file
    with open(file_path, 'w', encoding='utf-8') as outfile:
        
        # Writes the file as JSON
        json.dump(data, outfile, indent=4, 
                  ensure_ascii=False)
        logging.debug(logger_msg)


def load_auth():
    '''
    Loads a auth file and writes it to disk

    return: Auth dict
    '''
    
    auth_file = "deb/auth.json"
    token_old = 0
    hours_too_old = 2
    #auth['password'] = isBase64

    # Check if the file exists or not
    if check_exists(auth_file) == 0:

        raise "No authentication file!"

    # Open the auth file
    try:
        
        with open(auth_file, "r", encoding='utf-8') as f:
            
            auth = json.load(f)
            
    except Exception as e:
        
        print("ERROR OPENING AUTH.JSON: {}".format(e))

    if auth['last_used'] != "":
        # Check if the token is too old
        try:

            auth_time = datetime.datetime.strptime(auth['last_used'], "%Y-%m-%d %H:%M:%S.%f")
            time_now = datetime.datetime.now()
            
            hours_to_seconds = hours_too_old * 60
            
            if (time_now - auth_time).total_seconds() > hours_to_seconds:
                
                token_old = 1
            
        except:
            
            logging.warning("JSON parsing error when trying to access auth['last_used'] --> Exiting...")
            
            raise 'CRITICAL_TIME_JSON_ERROR'

    if auth['token'] == "":

        auth['token'] = login.get_token(auth)
        auth['last_used'] = str(datetime.datetime.now())

        write_json(auth, auth_file, "Auth file written to disk")

        return auth

    logging.debug("Token loaded from file: {}".format(auth['token']))


    return auth
    

def fetch_new_token(status=""):
    '''
    Fetches a new token if a token has expired and writes it to disk

    Attributes:

        * status (str) [default=""]: Has the token expired?

    return: Auth dict
    '''
    
    token_file = "deb/auth.json"
    
    auth = load_auth() # Load the auth file

    # Check if the token is expired
    if status == "EXPIRED":

        token = login.get_token(auth) # Gets the token
        auth['token'] = token # Write the token to auth

    # Write the datetime.now() to auth
    auth['last_used'] = str(datetime.datetime.now()) 

    # Write to disk
    write_json(auth, token_file, "Auth file written to disk")


    return auth
        

def write_error(base_path, error, assignment_id, file_format="log"):
    '''
    Writes error to a file for later analysis

    Attributes:

        * base_path (str): Base of the path
        * error (dict or str): Error to write to disk
        * assignment_id (str): Unqiue assignment identifer funciton as the errors file name

    return: Exits function
    '''

    file_path = "{}/error/{}.{}".format(base_path, assignment_id, file_format)
    logging.info("Writting error to file: {} | File_format: {}".format(file_path, file_format))

    with open(file_path, 'w', encoding="utf-8") as f:
    
        # Log format
        if file_format == "log":

                f.write(error)
                return

        # JSON
        elif file_format == "json":

            # Check if JSON
            if type(error) is dict:

                json.dump(error, f, ensure_ascii=False)
                return

    # PKL
    if file_format == "pkl":

        pickle.dump(error, open(file_path, 'wb'))
        return
    

def append_to_file(base_path, text):
    '''
    Appends some arbitrary data to a file

    Attributes:

        * base_path (str): Base of the path
        * text (str): Data to append
    '''

    file = "{}/error/file_type.txt".format(base_path)

    with open(file, 'a+') as f:

        f.write("{}\n".format(text))



def isBase64(sb):
    try:
        if isinstance(sb, str):
            # If there's any unicode here, an exception will be thrown and the function will return false
            sb_bytes = bytes(sb, 'ascii')
        elif isinstance(sb, bytes):
            sb_bytes = sb
        else:
            raise ValueError("Argument must be string or bytes")
        return base64.b64encode(base64.b64decode(sb_bytes)) == sb_bytes
    except Exception:
            return False
